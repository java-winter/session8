package company;

public class Payroll {
	private Employee[] employees;
	
	public Payroll(Employee[] employees) {
		this.employees = employees;
	}
	
	public void paySalary() {
		for (Employee emp : employees) {
			System.out.println("the salary of employee " + emp + " is " + emp.salary());
		}
	}
}
