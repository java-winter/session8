package company;

import java.util.ArrayList;
import java.util.List;

public class Company {
	
	public static void main(String[] args) {
		Employee[] empList = new Employee[3];
		
		try {
			Employee emp1 = new SalariedEmployee("Toto", "1234", -700.0);
			Employee emp2 = new HourlyEmployee("POPO", "7777", 40, 123);
			Employee emp3 = new ComissionEmployee("JOJO", "333", 20, 30);
			
			empList[0] = emp1;
			empList[1] = emp2;
			empList[2] = emp3;
			Payroll payroll = new Payroll(empList);
			payroll.paySalary();
		}
		catch(IllegalArgumentException exc) {
			System.out.println("An error happened" + exc.getMessage());
		}
		
		//Read about list and collection and arrayList in Java
//		List<Employee> lstEmp = new ArrayList<Employee>();
//		lstEmp.add(emp1);
//		lstEmp.add(emp2);
//		lstEmp.add(emp3);
//		lstEmp.add(new ComissionEmployee("JOJO", "333", 20, 30));
		

	}
}
