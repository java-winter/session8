package company;

public class SalariedEmployee extends Employee {
	
	protected double basicSalary;
	
	public SalariedEmployee(String name, String ssn, double basicSalary) {
		super(name, ssn);
		setBasicSalary(basicSalary);
	}

	public double getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(double basicSalary) {
//		if (basicSalary <= 0.0) {
//			throw new IllegalArgumentException("the salary needs to be more than 0");
//		}
//		this.basicSalary = basicSalary;
				
		if (basicSalary >= 0.0) {
			this.basicSalary = basicSalary;
		}
		else {
			throw new IllegalArgumentException("the salary needs to be more than 0");
		}
		
	}

	@Override
	public double salary() {
		return getBasicSalary();			
	}

	@Override
	public String toString() {
		return super.toString() +  "SalariedEmployee [basicSalary=" + basicSalary + "]";
	}
	
	
}
