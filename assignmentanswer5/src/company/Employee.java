package company;

public abstract class Employee {
	protected String name;
	protected String ssn;
	
	public Employee(String name, String ssn) {
		setName(name);
		setSsn(ssn);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", ssn=" + ssn + "]";
	}
	
	public abstract double salary();
}
