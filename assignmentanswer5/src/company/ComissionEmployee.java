package company;

public class ComissionEmployee extends Employee {
	protected double sales;
	protected double commision;
	
	public ComissionEmployee(String name, String ssn, double sales, double commision) {
		super(name, ssn);
//		this.sales = sales;
//		this.commision = commision;
		setSales(sales);
		setCommision(commision);
	}

	public double getSales() {
		return sales;
	}

	public void setSales(double sales) {
		this.sales = sales;
	}

	public double getCommision() {
		return commision;
	}

	public void setCommision(double commision) {
		this.commision = commision;
	}

	@Override
	public double salary() {	
		return getCommision() * getSales() * 1.5;
	}

	@Override
	public String toString() {
		return super.toString() +  "ComissionEmployee [sales=" + sales + ", commision=" + commision + "]";
	}
	
	
	
	
}
