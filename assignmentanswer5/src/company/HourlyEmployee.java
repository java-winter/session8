package company;

public class HourlyEmployee extends Employee {
	protected double hour;
	protected double wage;
	
	public HourlyEmployee(String name, String ssn, double hour, double wage) {
		super(name, ssn);
		setHour(hour);
		setWage(wage);
	}

	public double getHour() {
		return hour;
	}

	public void setHour(double hour) {
		this.hour = hour;
	}

	public double getWage() {
		return wage;
	}

	public void setWage(double wage) {
		if (wage >= 0.0) {
			this.wage = wage;
		}
		else {
			throw new IllegalArgumentException("the wage needs to be more than 0");
		}
	}

	@Override
	public double salary() {
		return getHour() * getWage();
	}

	@Override
	public String toString() {
		return super.toString() +   "HourlyEmployee [hour=" + hour + ", wage=" + wage + "]";
	}
	
	
	
}
