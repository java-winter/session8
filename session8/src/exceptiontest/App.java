package exceptiontest;

class Person{
	private String name;
	private int age;
	
	public Person(String name, int age) {
		setAge(age);
		setName(name);
	}
	
	public int getAge() {
		return this.age;
	}
	public void setAge(int age) {
		if (age > 1 && age < 120) {
			this.age = age;	
		}
		else {
			throw new IllegalArgumentException("age needs to be between 1 and 120");
		}
		
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		if (name.length() > 2 && name.length() < 250) {
			this.name = name;
		}
		else {
			throw new IllegalArgumentException("name length needs to be more than 2 and less than 250");
		}
		
	}
}

class Register{
	
	public void registerPerson(String name, int age) throws IllegalArgumentException {
		try {
			Person p = new Person(name, age);
		}
		catch(IllegalArgumentException exc) {
			throw exc;
		}
	}
}



public class App {

	public static void main(String[] args) {
		//Arithmetic exception
		try {
			int x = 50;
			int y = 0;
			System.out.println(x / y);
		}
		catch (ArithmeticException e) {//arithmetic is a child of exception
			//System.out.println(" an error happened because you were trying to do division by zero");
		}
		
		try {
			String str = null;			
			System.out.println(str.concat("my name"));
		}
		catch(NullPointerException exc) { //nullpointer is a child of exception
			//System.out.println("string is null");
		}
		
		
		try {
			Person p1 = new Person("Alex", 12);
			
			Person p2 = new Person("Betty", 40); //this line throws an exception
			
			Person p3 = new Person("ABC", -20);

		}
		catch (IllegalArgumentException ex) {
			System.out.println("an error happened " + ex.getMessage());
		}
		catch(Exception ex) {
			System.out.println("an error happened " + ex.getMessage());
		}
		finally {// our codes will come to this block either we have exception ot not.
			System.out.println("initialization finished");
		}
		
		
		Register r = new Register();
		r.registerPerson("ABC", -20);
		
		
		

	}

}
